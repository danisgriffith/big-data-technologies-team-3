from pyspark import SparkContext

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *

from custom_utils.pyspark_utils import PySparkHelper

from custom_hooks.mongo_hook import MongoHook

import pandas as pd
import numpy as np
import pytz


# debug 

WRITE_OUTPUT = True
WRITE_INPUT = True


# mongodb

MONGO_COLLECTION = 'twitter'

MONGO_CONN_ID_SYNC = 'mongo_cloud_layer4'
MONGO_URI_SYNC     = MongoHook(conn_id = MONGO_CONN_ID_SYNC).get_uri() + '.{0}'.format(MONGO_COLLECTION)

MONGO_CONN_ID_OUTPUT = 'mongo_cloud_layer5'
MONGO_URI_OUTPUT     = MongoHook(conn_id = MONGO_CONN_ID_OUTPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION)

print('MONGO_URI_SYNC   : ', MONGO_URI_SYNC)
print('MONGO_URI_OUTPUT : ', MONGO_URI_OUTPUT)


# pyspark helper

psh = PySparkHelper(debug = True, date_field = 'date_str')


# spark session

spark = SparkSession \
	.builder \
	.appName("airflow_spark_layer5_twitter") \
	.getOrCreate()


# spark context

sc = spark.sparkContext


# spark dataframe

print()
print('reading ...')

df = spark.read.format('mongo').load()
df_target = spark.read.format('mongo').option('uri', MONGO_URI_OUTPUT).load()


if (not df.rdd.isEmpty()):

	print()
	print('df.shape : ({0},{1})'.format(df.count(), len(df.columns)))
	print('df_target.shape : ({0},{1})'.format(df_target.count(), len(df_target.columns)))


	# filtering 

	df = psh.filter_synced(df)

	if (not df.rdd.isEmpty()):	


		# pandas dataframe

		print()
		print('converting to : Pandas DataFrame ...')

		df2 = df.toPandas()
		df2['date'] = pd.to_datetime(df2['date_str'], format = '%Y-%m-%d', exact = True)


		# interpolation

		print()
		print('interpolation ...')


		# interpolation (indexing)		

		df2_indexed = df2.set_index(keys = 'date', drop = False, inplace = False)


		# interpolation (new index)	

		start = df2['date'].min()
		end = df2['date'].max()

		print()
		print('start : ', start)
		print('end   : ', end)

		idx = pd.date_range(
			start = start,
			end = end,
			freq = 'D',
			tz = None
		)


		# interpolation (re-indexing)

		df2_reindexed = df2_indexed.reindex(idx)		


		# interpolation (in-filling)

		df_intp = df2_reindexed

		criteria = (df_intp.isnull()).any(axis = 1)
		criteria = criteria[criteria == True].index

		df_intp.loc[criteria, 'date'] = df_intp.loc[criteria].index

		df_intp.loc[criteria, 'date_str'] = df_intp.loc[criteria, 'date'].astype(str)
		df_intp.loc[criteria, 'date_day'] = (df_intp.loc[criteria, 'date'].dt.dayofweek) + 1
		df_intp.loc[criteria, 'date_weekend'] = (df_intp.loc[criteria, 'date_day'].isin([6,7]))

		df_intp.loc[criteria, 'year'] = (df_intp.loc[criteria, 'date'].dt.year).astype(str)
		df_intp.loc[criteria, 'month'] = (df_intp.loc[criteria, 'date'].dt.month).astype(str).str.zfill(2)
		df_intp.loc[criteria, 'year_month'] = df_intp.loc[criteria, 'year'] + ' - ' + df_intp.loc[criteria, 'month']

		df_intp.loc[criteria, 'synced'] = False

		cols = [
			'engagement',
			'favorite_count',
			'quote_count', 
			'reply_count', 
			'retweet_count'
		]
		df_intp.loc[criteria, cols] = df_intp.loc[criteria, cols].fillna(value = 0, inplace = False)

		
		print()
		print(df_intp[['date', 'date_day', 'quote_count', 'synced']].head())

		print()
		print(df_intp.dtypes)

		df2 = df_intp


		# selecting

		cols = ['_id', 'date']
		df2 = df2[ [c for c in df.columns if (c not in cols)] ]


		# spark dataframe

		print()
		print('converting to : Spark DataFrame ...')

		df3 = spark.createDataFrame(df2)
		

		# converting (data types are correct from the beginning)

		# ...


		if (not df3.rdd.isEmpty()):

			
			# join (input + target)

			df4 = psh.join(df3, df_target)		

			
			# writing (output collection)

			if (WRITE_OUTPUT):	
				psh.update_target(df4, df_target)				


			# writing (input collection)

			if (WRITE_INPUT):
				df = psh.sync_prep(df)
				psh.update_input(df, MONGO_URI_SYNC)