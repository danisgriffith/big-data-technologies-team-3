# dag
from airflow import DAG


# hooks
#from custom_hooks.mongo_hook import MongoHook
from airflow.hooks.base_hook import BaseHook


# sensors
from airflow.sensors.http_sensor import HttpSensor
from custom_sensors.mongo_sensor import MongoSensor


# operators
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator


# utils
from custom_utils.alphavantage_utils import AlphaVantageHelper


# others
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago

import json




# dag

default_args = {
    'owner': 'me',
    'depends_on_past': False,
    'start_date': days_ago(0),
    #'retries': 1,
    #'retry_delay': timedelta(minutes = 5)
}

dag = DAG(
    'layer0_ingestion_alphavantage_daily_full_v01', 
    default_args = default_args,
    schedule_interval = '@once'
    )


# alphavantage

HTTP_CONN_ID = "http_alphavantage"
BEARER_TOKEN = BaseHook.get_connection(conn_id = HTTP_CONN_ID).password
OUTPUT_SIZE_REQ = "full"
OUTPUT_SIZE_RSP = "Full size"
SYMBOL = "MSFT"


t_alphavantage_full_operator = SimpleHttpOperator(
    task_id ='alphavantage_full_operator',
    http_conn_id = HTTP_CONN_ID,
    endpoint = '/query?function=TIME_SERIES_DAILY&symbol={0}&outputsize={1}&apikey={2}'.format(SYMBOL, OUTPUT_SIZE_REQ, BEARER_TOKEN),
    method = 'GET',
    headers = {
        'cache-control':    'no-cache',
        'content-type':     'application/json'
    },    
    log_response = False,
    response_check = AlphaVantageHelper(symbol = SYMBOL, output_size = OUTPUT_SIZE_RSP).check_response,
    xcom_push = True,
    dag = dag    
)


# mongodb

MONGO_CONN_ID = "mongo_cloud_layer0"
MONGO_COLLECTION = "msft"
DATE_FIELD = "date"


def pull_replace(**kwargs):
    
    data_pulled = kwargs['ti'].xcom_pull(
        dag_id = 'layer0_ingestion_alphavantage_daily_full_v01', 
        task_ids = 'alphavantage_full_operator'
    )    

    avh = AlphaVantageHelper(symbol = SYMBOL, output_size = OUTPUT_SIZE_RSP, date_field = DATE_FIELD)

    if (data_pulled):
        docs = avh.data_prep(data = data_pulled)
        result = avh.replace(mongo_conn_id = MONGO_CONN_ID, mongo_collection = MONGO_COLLECTION, docs = docs)

        print("acknowledged   : ", result.acknowledged)
        print("deleted  count : ", result.deleted_count)
        print("inserted count : ", result.inserted_count)
        print("matched  count : ", result.matched_count)
        print("modified count : ", result.modified_count)
        print("upserted_ids   : ", result.upserted_ids)


t_mongo_sensor = MongoSensor(
    task_id = "mongo_sensor",
    mongo_conn_id = MONGO_CONN_ID,
    mongo_collection = MONGO_COLLECTION,
    soft_fail = False,
    timeout = 2,
    mode = "poke",
    poke_interval = 5,
    retries = 10,
    dag = dag
)

t_mongo_operator = PythonOperator(
    task_id = 'mongo_pull_store',
    provide_context = True,
    python_callable = pull_replace,
    retries = 1,
    dag = dag,
)


# trigger

def conditionally_trigger(context, dag_run_obj):
    return dag_run_obj


t_trigger_operator = TriggerDagRunOperator(
    task_id = 'trigger',
    dag = dag,
    trigger_dag_id = 'layer1_drop_alphavantage',
    python_callable = conditionally_trigger
)


# dependencies

t_alphavantage_full_operator >> t_mongo_sensor >> t_mongo_operator >> t_trigger_operator