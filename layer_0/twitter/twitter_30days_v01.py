# dag
from airflow import DAG

# sensors
from airflow.sensors.http_sensor import HttpSensor

# operators
from airflow.operators.http_operator import SimpleHttpOperator

# others
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago

import json


default_args = {
    'owner': 'me',
    'depends_on_past': False,
    'start_date': days_ago(1),
    #'retries': 1,
    #'retry_delay': timedelta(minutes = 5)
}

dag = DAG(
    'twitter_search_30days_v01', 
    default_args = default_args, 
    #schedule_interval='@daily'
    #tags = ['example']
    )


if (False):
    sensor = HttpSensor(
        task_id = 'http_twitter_search_30days_sensor',
        http_conn_id = 'http_twitter_1.1',
        method = 'GET',
        endpoint = '',
        timeout = 1,
        mode = 'poke',
        poke_interval = 10,
        dag = dag,
    )

def t1_get_dates():

    today_utc = datetime.utcnow()
    today_utc_date = today_utc.date()

    todate_date = today_utc_date - timedelta(days = 1)
    fromdate_date = todate_date - timedelta(days = 5 - 1)

    todate_str = todate_date.strftime('%Y%m%d')
    fromdate_str = fromdate_date.strftime('%Y%m%d')

    todate = todate_str + '2359'
    fromdate = fromdate_str + '0000'

    return fromdate, todate

#t1_fromdate, t1_todate = t1_get_dates()


def t1_check_response(response):

    try:
        r = response.json()
    except:
        return False
    else:
        condition = ( 
            (isinstance(r, dict))
            & ('errors' not in r)
            & ('error' not in r)
            & ('results' in r)
            & (isinstance(r['results'], list))
            )
        return condition



t1 = SimpleHttpOperator(
    task_id ='http_twitter_search_30days_post',
    http_conn_id = 'http_twitter',
    endpoint = '/1.1/tweets/search/30day/30D.json',
    method = 'POST',
    headers = {
        'cache-control':    'no-cache',
        'content-type':     'application/json',
        'authorization':    'Bearer AAAAAAAAAAAAAAAAAAAAAP%2F9DAEAAAAACZY09B5cWMKOP4GoazClAvgulZc%3DSySe5fzOSmIeSL1hDKWJ0y94T1j3Zdgq34HShmJtR7k3y7CfRy'
    },
    data = json.dumps({
        'query':        'from:satyanadella',
        'maxResults':   '100',
        'fromDate':     '202003200000',
        'toDate':       '202003252359'
    }),
    response_check = t1_check_response,
    xcom_push = True,
    dag = dag    
)


#sensor >> t1