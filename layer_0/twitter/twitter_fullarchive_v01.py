# dag
from airflow import DAG


# hooks
from custom_hooks.mongo_hook import MongoHook
from airflow.hooks.base_hook import BaseHook


# sensors
from airflow.sensors.http_sensor import HttpSensor
from custom_sensors.mongo_sensor import MongoSensor


# operators
from custom_operators.http_twitter_operator import TwitterHttpOperator
from airflow.operators.python_operator import PythonOperator


# others
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago

import json




# dag

default_args = {
    'owner': 'me',
    'depends_on_past': False,
    'start_date': days_ago(0),
    #'retries': 1,
    #'retry_delay': timedelta(minutes = 5)
}

dag = DAG(
    'twitter_fullarchive_v01', 
    default_args = default_args, 
    schedule_interval = None
    )


# twitter

HTTP_CONN_ID_SENSOR = "http_twitter"
HTTP_CONN_ID_OPERATOR = "http_twitter_api"
BEARER_TOKEN = BaseHook.get_connection(conn_id = HTTP_CONN_ID_OPERATOR).password
FROM_DATE = '201401010000'

def t1_get_dates():

    today_utc = datetime.utcnow()
    today_utc_date = today_utc.date()

    todate_date = today_utc_date - timedelta(days = 1)
    fromdate_date = todate_date - timedelta(days = 20 - 1)

    todate_str = todate_date.strftime('%Y%m%d')
    fromdate_str = fromdate_date.strftime('%Y%m%d')

    todate = todate_str + '2359'
    fromdate = fromdate_str + '0000'

    return fromdate, todate

def t1_check_response(response):

    try:
        r = response.json()
    except:
        return False
    else:
        condition = ( 
            (isinstance(r), dict)
            & ('results' in r)
            & (isinstance(r['results']), list)
            )
        return condition

t_twitter_sensor = HttpSensor(
    task_id = 'twitter_sensor',
    http_conn_id = HTTP_CONN_ID_SENSOR,
    method = 'GET',
    endpoint = '',
    timeout = 1,
    mode = 'poke',
    poke_interval = 10,
    dag = dag,
)

t_twitter_operator = TwitterHttpOperator(
    task_id ='twitter_operator',
    http_conn_id = HTTP_CONN_ID_OPERATOR,
    endpoint = '/tweets/search/fullarchive/FA.json',
    method = 'POST',
    headers = {
        'cache-control':    'no-cache',
        'content-type':     'application/json',
        'authorization':    'Bearer {0}'.format(BEARER_TOKEN)
    },
    data = json.dumps({
        'query':        'from:satyanadella',
        'maxResults':   '100',
        'fromDate':     '' + FROM_DATE + '',
        'toDate':       '' + t1_get_dates()[1] + ''
    }),
    #log_response = True,
    response_check = t1_check_response,
    xcom_push = True,
    dag = dag    
)


# mongodb

MONGO_CONN_ID = "mongo_cloud_layer0"
MONGO_COLLECTION = "twitter"

def replace(docs):
    hook = MongoHook(MONGO_CONN_ID)    
    result = hook.replace_many(
        mongo_collection = MONGO_COLLECTION, 
        docs = docs, 
        filter_docs = [{'id': doc['id']} for doc in docs], 
        upsert = True
        )

    print("REPLACE_MANY >> UPDATE RESULT")
    print("acknowledged : ", result.acknowledged)
    if (result.acknowledged):
        print("matched_count   : ", result.matched_count)
        print("modified_count  : ", result.modified_count)
        if (hasattr(result, "upserted_id")):
            print("upserted_id     : ", result.upserted_id)
        if (hasattr(result,"raw_result")):
            print("raw_result      : ", result.raw_result)

def pull_replace(**kwargs):
    data_pulled = kwargs['ti'].xcom_pull(
        dag_id = 'twitter_fullarchive_v01', 
        task_ids = 'twitter_operator'
    )

    print(type(data_pulled))
    print()

    if (data_pulled):
        x1 = str(data_pulled)
        print(type(x1))
        #print(x1)
        print()
        
        x2 = json.loads(x1)
        print(type(x2))
        print(x2[0]['created_at'])
        print()

        docs = json.loads(str(data_pulled))
        replace(docs)  

t_mongo_sensor = MongoSensor(
    task_id = "mongo_sensor",
    mongo_conn_id = MONGO_CONN_ID,
    mongo_collection = MONGO_COLLECTION,
    soft_fail = False,
    poke_interval = 5,
    timeout = 2,
    mode = "poke",
    dag = dag
    )

t_mongo_operator = PythonOperator(
    task_id = 'mongo_pull_store',
    provide_context = True,
    python_callable = pull_replace,
    dag = dag,
)

t_twitter_sensor >> t_twitter_operator >> t_mongo_sensor >> t_mongo_operator