# dag
from airflow import DAG


# hooks
#from custom_hooks.mongo_hook import MongoHook
from airflow.hooks.base_hook import BaseHook


# sensors
from airflow.sensors.http_sensor import HttpSensor
from custom_sensors.mongo_sensor import MongoSensor


# operators
from custom_operators.http_twitter_operator import TwitterHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator


# utils
from custom_utils.twitter_utils import TwitterHelper


# others
from airflow.utils.dates import days_ago

import json




# dag

default_args = {
    'owner': 'me',
    'depends_on_past': False,
    'start_date': days_ago(0),
    #'retries': 1,
    #'retry_delay': timedelta(minutes = 5)
}

dag = DAG(
    'layer0_ingestion_twitter_fullarchive_v02', 
    default_args = default_args, 
    schedule_interval = '@once'
    )


# twitter

#HTTP_CONN_ID_SENSOR = "http_twitter"

HTTP_CONN_ID_OPERATOR = "http_twitter_api"
HTTP_ENDPOINT_OPERATOR = '/tweets/search/fullarchive/FA.json'
BEARER_TOKEN = BaseHook.get_connection(conn_id = HTTP_CONN_ID_OPERATOR).password

th = TwitterHelper(mode = TwitterHelper.MODE_FULL)


t_twitter_operator = TwitterHttpOperator(
    task_id ='twitter_operator',
    http_conn_id = HTTP_CONN_ID_OPERATOR,
    endpoint = HTTP_ENDPOINT_OPERATOR,
    method = 'POST',
    headers = {
        'cache-control':    'no-cache',
        'content-type':     'application/json',
        'authorization':    'Bearer {0}'.format(BEARER_TOKEN)
    },
    data = json.dumps({
        'query':        'from:satyanadella',
        'maxResults':   '100',
        'fromDate':     '' + th.get_dates()[0] + '',
        'toDate':       '' + th.get_dates()[1] + ''
    }),
    log_response = False,
    #response_check = th.check_response,
    xcom_push = True,
    dag = dag    
)


# mongodb

MONGO_CONN_ID = "mongo_cloud_layer0"
MONGO_COLLECTION = "twitter"


def mongo_update(**kwargs):
    
    data_pulled = kwargs['ti'].xcom_pull(
        dag_id = 'layer0_ingestion_twitter_fullarchive_v02', 
        task_ids = 'twitter_operator'
    )

    if (data_pulled):
        docs = th.data_prep(data = data_pulled)
        result = th.replace(mongo_conn_id = MONGO_CONN_ID, mongo_collection = MONGO_COLLECTION, docs = docs)

        print("acknowledged   : ", result.acknowledged)
        print("deleted  count : ", result.deleted_count)
        print("inserted count : ", result.inserted_count)
        print("matched  count : ", result.matched_count)
        print("modified count : ", result.modified_count)
        print("upserted_ids   : ", result.upserted_ids)


t_mongo_sensor = MongoSensor(
    task_id = "mongo_sensor",
    mongo_conn_id = MONGO_CONN_ID,
    mongo_collection = MONGO_COLLECTION,
    soft_fail = False,
    poke_interval = 5,
    timeout = 2,
    mode = "poke",
    dag = dag
)

t_mongo_operator = PythonOperator(
    task_id = 'mongo_operator',
    provide_context = True,
    python_callable = mongo_update,
    dag = dag,
)


# trigger

def conditionally_trigger(context, dag_run_obj):
    return dag_run_obj


t_trigger_operator = TriggerDagRunOperator(
    task_id = 'trigger',
    dag = dag,
    trigger_dag_id = 'layer1_drop_twitter',
    python_callable = conditionally_trigger
)


# dependencies

t_twitter_operator >> t_mongo_sensor >> t_mongo_operator >> t_trigger_operator