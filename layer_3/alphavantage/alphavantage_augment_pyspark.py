from pyspark import SparkContext

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *

from custom_utils.pyspark_utils import PySparkHelper

from custom_hooks.mongo_hook import MongoHook

import pandas as pd


# debug 

WRITE_OUTPUT = True
WRITE_INPUT = True


# mongodb

MONGO_COLLECTION = 'msft'

MONGO_CONN_ID_SYNC = 'mongo_cloud_layer2'
MONGO_URI_SYNC     = MongoHook(conn_id = MONGO_CONN_ID_SYNC).get_uri() + '.{0}'.format(MONGO_COLLECTION)

MONGO_CONN_ID_OUTPUT = 'mongo_cloud_layer3'
MONGO_URI_OUTPUT     = MongoHook(conn_id = MONGO_CONN_ID_OUTPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION)

print(MONGO_URI_SYNC)
print(MONGO_URI_OUTPUT)


# pyspark helper

psh = PySparkHelper(debug = True, date_field = 'date')


# spark session

spark = SparkSession \
	.builder \
	.appName("airflow_spark_layer3_alphavantage") \
	.getOrCreate()


# spark context

sc = spark.sparkContext


# spark dataframe

print()
print('reading ...')

df = spark.read.format('mongo').load()
df_target = spark.read.format('mongo').option('uri', MONGO_URI_OUTPUT).load()


if (not df.rdd.isEmpty()):

	print()
	print('df.shape : ({0},{1})'.format(df.count(), len(df.columns)))
	print('df_target.shape : ({0},{1})'.format(df_target.count(), len(df_target.columns)))
	

	# filtering 

	df = psh.filter_synced(df)

	if (not df.rdd.isEmpty()):

		# pandas dataframe

		print()
		print('converting to : Pandas DataFrame ...')

		df2 = df.toPandas()


		# augmentation (date-related fields)

		print()
		print('augmentation : date-related fields ...')

		temp = pd.to_datetime(df2['date'], format = '%Y-%m-%d')

		df2['year'] = (temp.dt.year).astype(str)
		df2['month'] = (temp.dt.month).astype(str).str.zfill(2)
		df2['year_month'] = df2['year'] + '-' + df2['month']
		
		df2['date_str'] = temp.astype(str)
		df2['date_day'] = (temp.dt.dayofweek) + 1
		df2['date_weekend'] = (df2['date_day'].isin([6,7]))
		#df2['date'] = df2['date'].dt.date		


		# augmentation (differenced fields)

		#print()
		#print('augmentation : differenced fields ...')

		#periods = 1

		#df2['DL1'] = df2['close'].diff(periods = periods)
		#df2['VL1'] = df2.apply(func = lambda row: 'UP' if (row['DL1'] > 0) else 'DOWN', axis = 1)
		#df2 = df2[periods:]


		# spark dataframe

		print()
		print('converting to : Spark DataFrame ...')

		df2 = spark.createDataFrame(df2)


		# converting 		

		convert_dict = {
			'DecimalType' : ['open', 'high', 'low', 'close'],
			'IntegerType' : ['volume'],
			'BooleanType' : ['synced']
		}

		df3 = psh.convert(df2, convert_dict)


		# selecting

		cols = ['_id']
		mode = PySparkHelper.SELECT_MODE_EXCLUDE

		df3 = psh.select(df3, cols, mode)

		
		if (not df3.rdd.isEmpty()):

			# join (input + target)

			df4 = psh.join(df3, df_target)		


			# writing (output collection)

			if (WRITE_OUTPUT):	
				psh.update_target(df4, df_target)	


			# writing (input collection)

			if (WRITE_INPUT):
				df = psh.sync_prep(df)
				psh.update_input(df, MONGO_URI_SYNC)		