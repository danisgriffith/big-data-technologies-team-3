# -*- coding: utf-8 -*-

from airflow.hooks.base_hook import BaseHook
import json


class HttpTwitterPaginatedHook(BaseHook):
    
    
    def __init__(
            self,
            *args,
            #pagination_function = pagination_function,
            method = None,
            next_page_field = None,
            **kwargs
    ):

        if (not next_page_field):
            raise TypeError('next_page_field argument is expected, found None.')

        self.next_page_field = next_page_field
        super(HttpTwitterPaginatedHook, self).__init__(*args, method = method, **kwargs)


    def run(self, endpoint, data = None, headers = None, extra_options = None, **request_kwargs):
        """
        Performs the request. 
        Keep in mind future request might fail; therefore make sure to make the rest of your Task idempotent,
        so that you can manage failures correctly.
        
        :param endpoint: the endpoint to be called i.e. resource/v1/query?
        :type endpoint: str
        :param data: payload to be uploaded or request parameters
        :type data: dict
        :param headers: additional headers to be passed through as a dictionary
        :type headers: dict
        :param extra_options: additional options to be used when executing the request
            i.e. {'check_response': False} to avoid checking raising exceptions on non
            2XX or 3XX status codes
        :type extra_options: dict
        :param  **request_kwargs: Additional kwargs to pass when creating a request.
            For example, ``run(json=obj)`` is passed as ``requests.Request(json=obj)``
        
        :yields: A response object for every pages request.
        """

        if (not data):
            raise TypeError('data argument is expected, found None')

        
        response_list = []

        try:

            next_token = None
            first_call = True

            condition = ( (next_token == None) or (first_call) )

            while (condition):

                if (first_call):    
                    # updating while loop (partial) condition
                    first_call = False
                else:    
                    # updating dynamic settings before making 2nd, 3rd, ..., nth calls
                    if (next_token != None):

                        data_json = json.loads(data)
                        data_json['next'] = next_token

                        data = json.dumps(data_json)

                # command
                
                response = super(HttpTwitterPaginatedHook, self).run(
                    endpoint = endpoint,
                    data = data,
                    headers = headers,
                    extra_options = extra_options,
                    **request_kwargs
                )       
                
                rs = response.json()

                # appending each document within the response object to list

                if ('results' in rs):
                    if (isinstance(rs['results'], list)):
                        for resp in rs['results']:
                            response_list.append(resp)
                
                # updating while loop condition
                
                if ('next' in rs):
                    next_token = rs['next']
                else:
                    next_token = None

                condition = (next_token != None)

                #print('{0} records  |  next : {1}  |  continue : {2}'.format(len(response_list), next_token, condition))
        
        except Exception as ex:
            self.log.warning('Unknown error : ', str(ex))
        
        finally:
            return response_list

             


        