from pyspark import SparkContext

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *


class PySparkHelper():

	ID_FIELD = '_id'
	#DATE_FIELD = 'date_str'
	SYNC_FIELD = 'synced'
	
	SELECT_MODE_INCLUDE = 'INCLUDE'
	SELECT_MODE_EXCLUDE = 'EXCLUDE'

	FILTER_DATE_MODE_BEFORE  = 'BEFORE'
	FILTER_DATE_MODE_AFTER   = 'AFTER'
	FILTER_DATE_MODE_BETWEEN = 'BETWEEN'


	def __init__(
		self,
		debug,
		date_field
	):
		self.debug = debug,
		self.date_field = date_field


	def filter_synced(self, df):

		if (self.debug):
			print()
			print('pyspark helper : filtering ...')

		df2 = df.filter(df[PySparkHelper.SYNC_FIELD] == False)

		if (not df2.rdd.isEmpty()):
			print()
			print('pyspark helper : filtering >> df.shape = ({0},{1})'.format(df2.count(), len(df2.columns)))

		return df2

	def filter_by_date(self, df, date_field, date_min, date_max, mode):

		if (self.debug):
			print()
			print('pyspark helper : filtering by date ...')

		if ( (mode == PySparkHelper.FILTER_DATE_MODE_BEFORE) & (date_max != '') ):
			df2 = df.filter(df[date_field] <= date_max)
		
		elif ( (mode == PySparkHelper.FILTER_DATE_MODE_AFTER) & (date_min != '') ):
			df2 = df.filter(df[date_field] >= date_min)
		
		elif ( (mode == PySparkHelper.FILTER_DATE_MODE_BETWEEN) & (date_min != '') & (date_max != '') ):
			df2 = df.filter( ((df[date_field] >= date_min) & (df[date_field] <= date_max)) )

		if (not df2.rdd.isEmpty()):
			print()
			print('pyspark helper : filtering by date >> df.shape = ({0},{1})'.format(df2.count(), len(df2.columns)))

		return df2


	def select(self, df, cols, mode):

		if (self.debug):
			print()
			print('pyspark helper : selecting columns ...')

		if (mode == PySparkHelper.SELECT_MODE_INCLUDE):
			c_list = [c for c in df.columns if (c in cols)]
		elif (mode == PySparkHelper.SELECT_MODE_EXCLUDE):
			c_list = [c for c in df.columns if (c not in cols)]

		df2 = df.select(c_list)

		if (not df2.rdd.isEmpty()):
			print()
			print('pyspark helper : selecting columns >> df.shape = ({0},{1})'.format(df2.count(), len(df2.columns)))

		return df2


	def convert(self, df, convert_dict):

		if (self.debug):
			print()
			print('pyspark helper : conversion ...')

		df2 = df

		if (convert_dict):

			if ( 'DecimalType' in convert_dict):
				for c in convert_dict['DecimalType']:
					df2 = df2.withColumn(c, col(c).cast(DecimalType(precision = 9, scale = 2)))

			if ( 'IntegerType' in convert_dict):
				for c in convert_dict['IntegerType']:
					df2 = df2.withColumn(c, col(c).cast(IntegerType()))

			if ( 'BooleanType' in convert_dict):
				for c in convert_dict['BooleanType']:
					df2 = df2.withColumn(c, col(c).cast(BooleanType()))

		if (not df2.rdd.isEmpty()):
			print()
			print('pyspark helper : conversion >> df.shape = ({0},{1})'.format(df2.count(), len(df2.columns)))

		return df2


	def join(self, df1, df2):

		if (self.debug):
			print()
			print('pyspark helper : dataframes join (input + target) ...')

		if ( df2.rdd.isEmpty() ):
				df = df1
		else:					
			cols_select = [
				PySparkHelper.ID_FIELD,
				self.date_field
			]
			df2_temp = df2.select(cols_select)			
			df = df1.join(other = df2_temp, on = self.date_field, how = 'left')

		if (not df.rdd.isEmpty()):
			print()
			print('pyspark helper : dataframes join (input + target) >> df.shape = ({0},{1})'.format(df.count(), len(df.columns)))

		return df


	def update_target(self, df, df_target, overwrite = False):

		# DataFrameWriter<T> mode 	: 	specifies the behavior when data or table already exist;

		# overwrite 				: 	drop the existing collection and overwrite with new data;
		# append 					: 	append the data (will raise exception if collection does not exist);						
		# ignore 					: 	ignore the operation (i.e. no-op);
		# error or errorifexists 	: 	default option, throw an exception at runtime;


		# Specifi case:

		# overwrite 				: 	NO - it drop the target collection every time;
		# append					:	YES - by default it doesn't check for existing documents, resulting in multiple records per date;
		#									  but specifying the replaceDocument option we avoid this issue and, furthermore,
		#									  we only update the specific fields of existing documents (_'id' field)
		#									  and insert the former when there is no match ('_id' field)
		# ignore					:	NO - see above;
		# error or errorifexists 	: 	NO - see above;
		

		if (self.debug):
			print()
			print('pyspark helper : updating target ...')

		if (df.rdd.isEmpty()):
			return

		if ( df_target.rdd.isEmpty() ):
			df.write.format("mongo").mode("overwrite").save()
		else:
			if (overwrite):
				df.write.format("mongo").mode("overwrite").save()
			else:
				df.write.format("mongo").mode('append').option('replaceDocument', 'false').save()

		print()
		print('pyspark helper : updating target >> # records = {0}'.format(df.count()))

		return


	def append_target(self, df, df_target):		

		if (self.debug):
			print()
			print('pyspark helper : appending target ...')

		if (df.rdd.isEmpty()):
			return

		if ( df_target.rdd.isEmpty() ):
			df.write.format("mongo").mode("overwrite").save()
		else:
			df.write.format("mongo").mode('append').save()

		print()
		print('pyspark helper : appending target >> # records = {0}'.format(df.count()))

		return


	def sync_prep(self, df):

		if (self.debug):
			print()
			print('pyspark helper : sync data prep ...')

		cols_sync = [
			PySparkHelper.ID_FIELD,
			PySparkHelper.SYNC_FIELD,
		]

		df2 = df \
				.select(cols_sync) \
				.withColumn(PySparkHelper.SYNC_FIELD, lit(True)) 

		df2.show()

		if (not df2.rdd.isEmpty()):
			print()
			print('pyspark helper : sync data prep >> df.shape = ({0},{1})'.format(df2.count(), len(df2.columns)))

		return df2


	def update_input(self, df, uri):

		if (self.debug):
			print()
			print('pyspark helper : updating input ...')

		if (df.rdd.isEmpty()):
			return

		l =  df.count()

		df.write \
			.format("mongo") \
			.mode('append') \
			.option('uri', uri) \
			.option('replaceDocument', 'false') \
			.save()		

		print()
		print('pyspark helper : updating input >> # records = {0}'.format(l))

		return




