import json
import pandas as pd
import numpy as np

import datetime

import os


class AppHelper():


	# TWEETS ENGAGEMENT LEVEL 

	DICT_LOW = {
		'T_MIN': 10,
		'T_MAX': 1000
	}


	DICT_MID = {
		'T_MIN': 1000,
		'T_MAX': 2000
	}


	DICT_HIGH = {
		'T_MIN': 2000,
		'T_MAX': 999999
	}


	# CORRELATION

	CORRELATION_MODE_PEARSON = 'pearson'

	N_IN  = 7
	N_OUT = 14
	LAGS  = 10

	THRESHOLD = 0.6
	TWEETS_FRACTION_SIGNIFICANCE_TRESHOLD = 0.04


	TEMPLATE_DIR = os.environ['AIRFLOW_HOME'] + '/dags/app/static/'


	def __init__(
		self,
		debug = False
	):
		self.debug = debug


	def dataframe_split(self, df, threshold_field, t_min, t_max):

		if (self.debug):
			print()
			print('app helper : dataframe_split ...')

		criteria = ( df[threshold_field].between(left = t_min, right = t_max, inclusive=True) )
		df_t = df[criteria]

		dates = []    
		dates.extend(df_t.index.tolist())
		l = len(dates)    

		df_list = []
	        
	    
		for c in range(l) :

			date_curr = dates[c]

			start = date_curr - pd.Timedelta(days = 30*2)
			end = date_curr + pd.Timedelta(days = 30*2)

			df_temp = df[start:end]            
			df_list.append({'date': date_curr, 'df': df_temp})       

		return df_list


	def get_range(self, x_start, x_end, lag, lower_bound, upper_bound):
    
		X_START = x_start
		X_END   = x_end      

		LOWER_BOUND = lower_bound
		UPPER_BOUND = upper_bound

		y_range = pd.date_range(
			start = X_START + pd.Timedelta(days = lag),
			end   = X_END   + pd.Timedelta(days = lag),
			freq = 'D'
		)

		condition = ((y_range.min() >= LOWER_BOUND) & (y_range.max() <= UPPER_BOUND))
		if (condition):
			return y_range
		else:
			return None


	def correlation_test_01(self, date, df, x_field, y_field, lag, engagement_level, method):

		if (self.debug):
			print()
			print('app helper : correlation_test_01 ...')

		df_temp  = df.copy()


		# parameters 

		DATE_LOWER_BOUND = df_temp.index.min() + pd.Timedelta(days = AppHelper.N_IN)
		DATE_UPPER_BOUND = df_temp.index.max() - pd.Timedelta(days = AppHelper.N_OUT) - pd.Timedelta(days = lag)


		# preliminary check

		if (self.debug):
			print()
			print('app helper : correlation_test_01 >> preliminary check ...')

		if ( (date <= DATE_LOWER_BOUND) | (date >= DATE_UPPER_BOUND) ):
			dict_temp = {
				'exec_date'			: datetime.datetime.utcnow().date().strftime("%Y-%m-%d"),
				'method'			: method,
				'lag'				: lag,
				'corr'				: 0,
				'engagement_level'	: engagement_level
			}
			return dict_temp


		# x series

		x_start = date - pd.Timedelta(days = AppHelper.N_IN)
		x_end   = date + pd.Timedelta(days = AppHelper.N_OUT)

		x_series = df_temp[x_field].astype(int).diff().dropna()    
		x = x_series[x_start:x_end]
		x_np = x.to_numpy()


		# y range

		y_range = self.get_range(
			x_start = x_start, 
			x_end   = x_end, 
			lag     = lag, 
			lower_bound = df_temp.index.min(),
			upper_bound = df_temp.index.max()
		)       


		# y series

		y_series = df_temp[y_field].diff().dropna()

		y_start = y_range.min()
		y_end   = y_range.max()

		y = y_series[y_start:y_end]
		y_np = y.to_numpy()


		# correlation

		if (self.debug):
			print()
			print('app helper : correlation_test_01 >> correlation ...')

		if (method == AppHelper.CORRELATION_MODE_PEARSON):   
			#print('x_np : ', x_np)
			#print('y_np : ', y_np) 
			correlation = np.corrcoef(x_np,y_np)[0][1]
			if (correlation):
				correlation = round(correlation, 2)

		if (False):
			print()
			print('date : {0}  |  x : {1} {2}  |  y : {3} {4}  |  corr : {5}'
				.format(
				date.to_pydatetime().strftime("%Y-%m-%d"), 
				x.index.min().to_pydatetime().strftime("%Y-%m-%d"), 
				x.index.max().to_pydatetime().strftime("%Y-%m-%d"), 
				y_range.min().to_pydatetime().strftime("%Y-%m-%d"), 
				y_range.max().to_pydatetime().strftime("%Y-%m-%d"), 
				correlation
				)
			)

		dict_temp = {
			'exec_date'			: datetime.datetime.utcnow().date().strftime("%Y-%m-%d"),
			'method'			: method,
			'lag'				: lag,
			'corr'				: correlation,
			'engagement_level'	: engagement_level
		}

		return dict_temp


	def compute_correlation(self, df, dict_t, engagement_level, method, debug):

		#START_YEAR = str(int(datetime.date.today().year) - 3)
		#print('compute_correlation : ', df.columns)		

		lags_list = list(range(1, AppHelper.LAGS + 1))
		lags_list.extend([12, 14, 16, 18, 20])

		df_list = self.dataframe_split(
			df = df,#[START_YEAR:],
			threshold_field = 'engagement', 
			t_min = dict_t['T_MIN'],
			t_max = dict_t['T_MAX']
		)
		print()
		print('df_list : ', len(df_list))

		dict_list = []
		for dict_temp in df_list:

			date = dict_temp['date']
			df_t = dict_temp['df']

			for lag in lags_list:            

				dict_temp = self.correlation_test_01(
					date = date, 
					df = df_t, 
					x_field = 'engagement', 
					y_field = 'close',   
					lag = lag, 
					engagement_level = engagement_level,
					method = method
				)
				dict_list.append(dict_temp)


		# CORRELATION DATAFRAME 1 (RAW RESULTS)

		if (debug):
			print()
			print('app helper : compute_correlation >> raw results...')

		df_corr1 = pd.DataFrame(data = dict_list) 


		# CORRELATION DATAFRAME 2 (SIGNIFICANT RESULTS)

		if (debug):
			print()
			print('app helper : compute_correlation >> significant results...')

		criteria = (df_corr1['corr'].abs() >= AppHelper.THRESHOLD)
		df_corr2 = df_corr1[criteria]
		df_corr2['corr_abs'] = df_corr2['corr'].abs()
		df_corr2['corr_sign'] = df_corr2['corr'].apply(lambda x: 'positive' if (x >= 0) else 'negative')
		df_corr2 = df_corr2.sort_values(by = ['corr_abs', 'lag'], axis = 0, ascending = False, inplace = False)


		# CORRELATION DATAFRAME 3 (SIGNIFICANT RESULTS, AGGREGATED)

		if (debug):
			print()
			print('app helper : compute_correlation >> significant results (aggregated) ...')

		cols = ['method', 'lag', 'engagement_level', 'corr_sign', 'corr']
		df_corr3 = df_corr2[cols].groupby(
			by = ['method', 'lag', 'engagement_level', 'corr_sign'], 
			axis = 0, 
			as_index = False
		).count()
		df_corr3.rename(columns = {'corr' : 'count'}, inplace = True)
		df_corr3['tweets_fraction'] = round( (df_corr3['count'] / len(df_list)), 2)


		# CORRELATION DATAFRAME 4 (SIGNIFICANT TWEETS FRACTIONS)

		if (debug):
			print()
			print('app helper : compute_correlation >> significant tweets fractions ...')

		criteria = (df_corr3['tweets_fraction'] > AppHelper.TWEETS_FRACTION_SIGNIFICANCE_TRESHOLD)
		df_corr4 = df_corr3[criteria]
	    

		# DISPLAY 

		#print(df_corr2)
		#print(df_corr3)
		#print(df_corr4)

		return df_corr4


	def get_engagement_level(self, x):

		eng_level = None
		
		if ((x['engagement'] >= AppHelper.DICT_LOW['T_MIN']) & (x['engagement'] <= AppHelper.DICT_LOW['T_MAX'])):
		    eng_level = 'low'
		elif ((x['engagement'] >= AppHelper.DICT_MID['T_MIN']) & (x['engagement'] <= AppHelper.DICT_MID['T_MAX'])):
		    eng_level = 'middle'
		elif ((x['engagement'] >= AppHelper.DICT_HIGH['T_MIN']) & (x['engagement'] <= AppHelper.DICT_HIGH['T_MAX'])):
		    eng_level = 'high'        
		
		return eng_level 


	def get_prioritized_df(self, df):

		if (self.debug):
			print()
			print('app helper : get_prioritized_df ...')

		df_temp = pd.DataFrame()

		x = df.iloc[0]
		start = x['est_date_end']


		if (x['engagement_level'] == 'high'):
			higher_pr_levels = ['high']
		elif (x['engagement_level'] == 'middle'):
			higher_pr_levels = ['high', 'middle']
		elif (x['engagement_level'] == 'low'):
			higher_pr_levels = ['high', 'middle', 'low']


		criteria_tt = (
			(df['est_date_begin'] >= x['est_date_begin'])
			& (df['est_date_begin'] <= x['est_date_end'])
		)
		df_tt = df[criteria_tt]


		# ENGAGEMENT LEVEL - HIGH

		if (self.debug):
			print()
			print('app helper : get_prioritized_df >> high engagement level ...')

		criteria_tt_0 = (df_tt['engagement_level'] == higher_pr_levels[0])
		df_tt_0 = df_tt[criteria_tt_0]
    
		if (not df_tt_0.empty):
			if (len(df_tt_0) == 1):
				df_temp = df_temp.append(other = df_tt_0.iloc[[0]], ignore_index = False)
			elif (len(df_tt_0) > 1):
				df_temp = df_temp.append(other = df_tt_0.iloc[[0]], ignore_index = False)
				df_temp_inner = get_prioritized_df(df = df_tt_0.iloc[1:])
				df_temp = df_temp.append(other = df_temp_inner, ignore_index = False)   

			#print('return HIGH')
			#print(df_temp)

		else:

			# ENGAGEMENT LEVEL - MIDDLE

			if (self.debug):
				print()
				print('app helper : get_prioritized_df >> mid engagement level ...')

			criteria_tt_1 = (df_tt['engagement_level'] == higher_pr_levels[1])
			df_tt_1 = df_tt[criteria_tt_1]
			
			if (not df_tt_1.empty):
				if (len(df_tt_1) == 1):
					df_temp = df_temp.append(other = df_tt_1.iloc[[0]], ignore_index = False)
				elif (len(df_tt_1) > 1):
					df_temp = df_temp.append(other = df_tt_1.iloc[[0]], ignore_index = False)
					df_temp_inner = get_prioritized_df(df = df_tt_1.iloc[1:])
					df_temp = df_temp.append(other = df_temp_inner, ignore_index = False)   

			#print('return MID')
			#print(df_temp)

			else:

				# ENGAGEMENT LEVEL - LOW

				if (self.debug):
					print()
					print('app helper : get_prioritized_df >> low engagement level ...')

				criteria_tt_2 = (df_tt['engagement_level'] == higher_pr_levels[2])
				df_tt_2 = df_tt[criteria_tt_2]

				if (not df_tt_2.empty):
					if (len(df_tt_2) == 1):
						df_temp = df_temp.append(other = df_tt_2.iloc[[0]], ignore_index = False)
					elif (len(df_tt_1) > 1):
						df_temp = df_temp.append(other = df_tt_2.iloc[[0]], ignore_index = False)
						df_temp_inner = get_prioritized_df(df = df_tt_2.iloc[1:])
						df_temp = df_temp.append(other = df_temp_inner, ignore_index = False)

				#print('return LOW')
				#print(df_temp)

		return df_temp


	def recommendation_test_01(self, df_temp, df_corr):

		if (self.debug):
			print()
			print('app helper : recommendation_test ...')

		#print(df_temp[['engagement', 'engagement_level']])
		#print(df_corr)


		# 2) JOIN WITH CORRELATION TABLE ON [ENGAGEMENT_LEVEL]

		if (self.debug):
			print()
			print('app helper : recommendation_test >> join with correlation table ...')

		df_temp2 = df_temp[['date', 'engagement', 'engagement_level']].merge(
			right = df_corr[['lag', 'corr_sign', 'engagement_level']], 
			how = 'inner', 
			on = ['engagement_level'], 
			left_index = False, 
			right_index = False, 
			copy = True
		)        
		df_temp2 = df_temp2.set_index('date', drop = False)

		#print('----------------------------------------------')
		#print(df_temp2)


		# 3) ESTIMATE EACH TWEET EFFECT ONTO MSFT STOCK PRICE (BEGIN DATE, END DATE)

		if (self.debug):
			print()
			print('app helper : recommendation_test >> estimating tweets effect (date range) ...')

		df_temp2['est_date_begin'] = df_temp2.apply(lambda x: x['date'] + pd.Timedelta(days = x['lag']), axis = 1)
		df_temp2['est_date_end'] = df_temp2.apply(lambda x: x['est_date_begin'] + pd.Timedelta(days = 7), axis = 1)
		df_temp2.sort_values(
			by = ['est_date_begin', 'engagement'], 
			axis = 0, 
			ascending = [True, False], 
			inplace = True
		)

		#print('----------------------------------------------')
		#print(df_temp2)


		# 4) FILTER OUT ALL THOSE TWEETS FOR WHICH THE ESTIMATED BEGINNING OF EFFECT IS IN THE PAST (INCLUDING TODAY)

		if (self.debug):
			print()
			print('app helper : recommendation_test >> filtering out unrelevant tweets ...')

		lower_bound = pd.Timestamp(datetime.datetime.today().strftime('%Y-%m-%d'))
		#lower_bound = df_temp2.index.max()
		criteria = (df_temp2['est_date_begin'] > lower_bound)
		df_temp2 = df_temp2[criteria]

		#print('----------------------------------------------')
		print(df_temp2)


		# 5) COMPUTING TWEETS ESTIMATED EFFECT

		if (self.debug):
			print()
			print('app helper : recommendation_test >> estimating tweets effect (most important only) ...')

		df_pr = self.get_prioritized_df(df = df_temp2)
		#print('----------------------------------------------')
		print(df_pr)


		# 6) PROVIDING RECOMMENDATIONS (IN THE FORM OF DATE RANGES)

		if (self.debug):
			print()
			print('app helper : recommendation_test >> providing recommendations ...')

		df_6 = df_pr.reset_index(drop = True)
		
		a = df_6[:-1]['corr_sign']
		b = df_6.shift(-1)[:-1]['corr_sign']

		criteria = ( a == b )
		changes = df_6[:-1][criteria == False]

		df_6['recommendation'] = None

		if (not changes.empty):
			changes = changes.iloc[0]
			#print(changes)

			upper_bound = changes['est_date_begin']
			print('upper_bound : ', upper_bound)
			criteria = (df_6['est_date_begin'] <= upper_bound)
			print(df_6[criteria])

			signs = df_6[criteria]['corr_sign'].unique().tolist()
			if (len(signs) == 1):
				df_6.loc[criteria,'recommendation'] = df_6.loc[criteria].apply(
					lambda x: 'long' if (x['corr_sign'] == 'positive') else 'negative', axis = 1)
		else:
			df_6['recommendation'] = df_6.apply(
				lambda x: 'long' if (x['corr_sign'] == 'positive') else 'negative', axis = 1)

		print(df_6)

		criteria = (df_6['recommendation'].notnull())
		df_6 = df_6[criteria]

		recomm_date_start = datetime.datetime.today().strftime('%Y-%m-%d')
		recomm_date_end   = df_6['est_date_begin'].max().strftime('%Y-%m-%d')
		recomm            = df_6['recommendation'].unique().tolist()[0]

		recomm_dict = {
			'exec_date_str'		: recomm_date_start,
			'start'				: recomm_date_start,
			'end'				: recomm_date_end,
			'recommendation'	: recomm
		}

		print()
		print(recomm_dict)


		return recomm_dict


	def get_template_inner(self):
		with open(AppHelper.TEMPLATE_DIR + 'template_inner.html', 'r') as file:
			template_inner = file.read()
		return template_inner


	def get_template_outer(self):
		with open(AppHelper.TEMPLATE_DIR + 'template_outer.html', 'r') as file:
			template_outer = file.read()
		return template_outer


	def build_recommendation_html(self, recommendation_list):

		if (True):
			import os
			print(AppHelper.TEMPLATE_DIR)
			for x in os.listdir(AppHelper.TEMPLATE_DIR):
				print(x)


		template_inner = self.get_template_inner()

		template_inner_final = ''
		for r in recommendation_list:
			templated_r = template_inner \
				.replace('{{exec_date}}', r['exec_date_str']) \
				.replace('{{start_date}}', r['start']) \
				.replace('{{end_date}}', r['end']) \
				.replace('{{recommendation}}', r['recommendation'])
			template_inner_final = template_inner_final + templated_r

		template_outer = self.get_template_outer()
		html_content = template_outer.replace('{{template_inner}}', template_inner_final)

		return html_content