# hooks
from custom_hooks.mongo_hook import MongoHook


# others
import json


class AlphaVantageHelper():

	TIME_SERIES_FIELD = 'Time Series (Daily)'
	SYNC_FIELD = 'synced'

	def __init__(
		self,
		symbol,
		output_size,
		date_field = None,
		debug = False
	):
		self.symbol = symbol
		self.output_size = output_size
		#if (not time_series_field):
		#	self.time_series_field = TIME_SERIES_FIELD
		#else:
		#	self.time_series_field = time_series_field
		self.date_field = date_field
		self.debug = debug


	def check_response(self, response):

		if (not response):
			return False

		r = json.loads(response.text)

		if (self.debug):
			print("isinstance(r, dict) 							: ", isinstance(r, dict))
			print("'Meta Data' in r.keys() 						: ", 'Meta Data' in r.keys())
			print("isinstance(r['Meta Data'], dict) 			: ", isinstance(r['Meta Data'], dict))
			print("'2. Symbol' in r['Meta Data'].keys() 		: ", '2. Symbol' in r['Meta Data'].keys())
			print("r['Meta Data']['2. Symbol'] 					: ", r['Meta Data']['2. Symbol'])
			print("'4. Output Size' in r['Meta Data'].keys() 	: ", '4. Output Size' in r['Meta Data'].keys())
			print("r['Meta Data']['4. Output Size'] 			: ", r['Meta Data']['4. Output Size'])
			print("'Time Series (Daily)' in r.keys()    		: ", AlphaVantageHelper.TIME_SERIES_FIELD in r.keys())
			print("isinstance(r['Time Series (Daily)'], dict) 	: ", isinstance(r[AlphaVantageHelper.TIME_SERIES_FIELD], dict))

		condition = (
			isinstance(r, dict)
			& ('Meta Data' in r.keys())
			& isinstance(r['Meta Data'], dict)
			& ('2. Symbol' in r['Meta Data'].keys())
			& (r['Meta Data']['2. Symbol'] == self.symbol)
			& ('4. Output Size' in r['Meta Data'].keys())
			& (r['Meta Data']['4. Output Size'] == self.output_size)
			& (AlphaVantageHelper.TIME_SERIES_FIELD in r.keys())  
			& isinstance(r[AlphaVantageHelper.TIME_SERIES_FIELD], dict)
		)

		return condition


	def data_prep(self, data, n = None):

		# w is a json array of arrays
		# x is a json array of arrays, with date injected as a field
		# y is a list of json documents
		# z is a list of josn documents, with renamed fields
		# docs is a sorted subset of z

		w = json.loads(str(data))

		x = w
		for (key, value) in x[AlphaVantageHelper.TIME_SERIES_FIELD].items():
			x[AlphaVantageHelper.TIME_SERIES_FIELD][key][self.date_field] = key


		y = [value for value in x[AlphaVantageHelper.TIME_SERIES_FIELD].values()]

		z = y
		for e in z:
			e['open']   = e.pop('1. open')
			e['high']   = e.pop('2. high')
			e['low']    = e.pop('3. low')
			e['close']  = e.pop('4. close')
			e['volume'] = e.pop('5. volume')
			e[AlphaVantageHelper.SYNC_FIELD] = False


		if (self.debug):
			print(z[:2])

		if (not n):
			docs = z
		else:
			docs = z[:n]
		
		return docs


	def replace(self, mongo_conn_id, mongo_collection, docs):

		hook = MongoHook(mongo_conn_id)    
		result = hook.replace_many(
			mongo_collection = mongo_collection, 
			docs = docs, 
			filter_docs = [{self.date_field: doc[self.date_field]} for doc in docs], 
			upsert = True
		)

		if (self.debug):
			print("Replace Many >> Update Result")
			print("acknowledged : ", result.acknowledged)
			if (result.acknowledged):
				print("deleted  count : ", result.deleted_count)
				print("inserted count : ", result.inserted_count)
				print("matched  count : ", result.matched_count)
				print("modified count : ", result.modified_count)
				if (hasattr(result, "upserted_id")):
					print("upserted_id     : ", result.upserted_id)
				if (hasattr(result,"raw_result")):
					print("raw_result      : ", result.raw_result)

		return result

