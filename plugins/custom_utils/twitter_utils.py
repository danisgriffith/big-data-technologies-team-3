# hooks
from custom_hooks.mongo_hook import MongoHook


# others
from datetime import datetime, timedelta
import json


class TwitterHelper():

	ID_FIELD = 'id'
	SYNC_FIELD = 'synced'

	MODE_FULL = 'FULL'
	MODE_30DAYS = '30DAYS'

	MODE_FULL_FROMDATE = '201310010000'

	#MODE_30DAYS_WINDOW = 30
	MODE_30DAYS_WINDOW = 30


	def __init__(
		self,
		mode,
		debug = False
	):
		self.mode = mode
		self.debug = debug


	def get_dates(self):

		today_utc = datetime.utcnow()
		today_utc_date = today_utc.date()

		todate_date = today_utc_date - timedelta(days = 1)
		todate_str = todate_date.strftime('%Y%m%d')
		todate = todate_str + '2359'

		if (self.mode == TwitterHelper.MODE_FULL):
			fromdate =  TwitterHelper.MODE_FULL_FROMDATE
		elif(self.mode == TwitterHelper.MODE_30DAYS):			
			fromdate_date = todate_date - timedelta(days = self.MODE_30DAYS_WINDOW - 1)
			fromdate_str = fromdate_date.strftime('%Y%m%d')
			fromdate = fromdate_str + '0000'		

		print(fromdate, todate)

		return fromdate, todate


	def check_response(self, response):

		if (not response):
			return False

		try:
			r = response.json()

		except:
			return False

		else:

			if (self.debug):
				print("isinstance(r, dict) 					: ", isinstance(r, dict))
				print("'errors' not in r 					: ", 'errors' not in r)
				print("'error' not in r 					: ", 'error' not in r)
				print("'results' in r 						: ", 'results' in r)
				print("isinstance(r['results'], list) 		: ", isinstance(r['results'], list))

			condition = ( 
				(isinstance(r, dict))
				& ('errors' not in r)
				& ('error' not in r)
				& ('results' in r)
				& (isinstance(r['results'], list))
			)
			
			return condition


	def data_prep(self, data, n = None):

		# x is a string
		# y is a list of json documents
		# z is a list of json documents, with the additional SYNC_FIELD

		if (data):

			x = str(data)
			#print(type(x))

			y = json.loads(x)
			#print(type(y))
			#print(y[0]['created_at'])

			z = y
			for e in z:
				e[self.SYNC_FIELD] = False

			if (self.debug):
				print(z[:2])

			if (not n):
				docs = z
			else:
				docs = z[:n]

			return docs


	def replace(self, mongo_conn_id, mongo_collection, docs):

		hook = MongoHook(mongo_conn_id)    
		result = hook.replace_many(
			mongo_collection = mongo_collection, 
			docs = docs, 
			filter_docs = [{self.ID_FIELD: doc[self.ID_FIELD]} for doc in docs], 
			upsert = True
		)

		if (self.debug):
			print("Replace Many >> Update Result")
			print("acknowledged : ", result.acknowledged)
			if (result.acknowledged):
				print("deleted  count : ", result.deleted_count)
				print("inserted count : ", result.inserted_count)
				print("matched  count : ", result.matched_count)
				print("modified count : ", result.modified_count)
				if (hasattr(result, "upserted_id")):
					print("upserted_id     : ", result.upserted_id)
				if (hasattr(result,"raw_result")):
					print("raw_result      : ", result.raw_result)

		return result

