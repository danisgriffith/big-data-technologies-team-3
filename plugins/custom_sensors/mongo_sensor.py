#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

# https://github.com/apache/airflow/blob/master/airflow/providers/mongo/sensors/mongo.py


from custom_hooks.mongo_hook import MongoHook
from airflow.sensors.base_sensor_operator import BaseSensorOperator
from airflow.utils.decorators import apply_defaults


class MongoSensor(BaseSensorOperator):
    """
    Checks for the existence of a document which
    matches the given query in MongoDB. Example:
    >>> mongo_sensor = MongoSensor(collection="coll",
    ...                            query={"key": "value"},
    ...                            mongo_conn_id="mongo_default",
    ...                            task_id="mongo_sensor")
    :param mongo_db: Target MongoDB database.
    :type mongo_db: str
    :param mongo_collection: Target MongoDB collection.
    :type mongo_collection: str
    :param query: The query to find the target document.
    :type query: dict
    :param mongo_conn_id: The connection ID to use
        when connecting to MongoDB.
    :type mongo_conn_id: str
    """
    #template_fields = ('collection', 'query')

    @apply_defaults
    def __init__(self, mongo_collection, mongo_conn_id = "mongo_default", *args, **kwargs):
        super(MongoSensor, self).__init__(*args, **kwargs)
        self.mongo_conn_id = mongo_conn_id
        self.mongo_collection = mongo_collection
        
        #self.log.info('MongoSensor created')

    def poke(self, context):
        hook = MongoHook(self.mongo_conn_id)
        self.log.info("MongoSensor checking the existence of collection : {0}.{1}".format(hook.connection.schema, self.mongo_collection))
        return hook.get_collection_name(mongo_collection = self.mongo_collection)
        #return hook.find(self.collection, self.query, find_one=True) is not None