from airflow.models import BaseOperator
#from airflow.operators.http_operator import SimpleHttpOperator
from airflow.hooks.http_hook import HttpHook
from airflow.utils.decorators import apply_defaults

import json

class TwitterHttpOperator(BaseOperator):
#class TwitterHttpOperator(SimpleHttpOperator):

	"""
    Calls an endpoint on an HTTP system to execute an action

    :param http_conn_id: The connection to run the operator against
    :type http_conn_id: str
    :param endpoint: The relative part of the full url. (templated)
    :type endpoint: str
    :param method: The HTTP method to use, default = "POST"
    :type method: str
    :param data: The data to pass. POST-data in POST/PUT and params
        in the URL for a GET request. (templated)
    :type data: For POST/PUT, depends on the content-type parameter,
        for GET a dictionary of key/value string pairs
    :param headers: The HTTP headers to be added to the GET request
    :type headers: a dictionary of string key/value pairs
    :param response_check: A check against the 'requests' response object.
        Returns True for 'pass' and False otherwise.
    :type response_check: A lambda or defined function.
    :param extra_options: Extra options for the 'requests' library, see the
        'requests' documentation (options to modify timeout, ssl, etc.)
    :type extra_options: A dictionary of options, where key is string and value
        depends on the option that's being modified.
    :param xcom_push: Push the response to Xcom (default: False).
        If xcom_push is True, response of an HTTP request will also
        be pushed to an XCom.
    :type xcom_push: bool
    :param log_response: Log the response (default: False)
    :type log_response: bool
    """

	#template_fields = ['endpoint', 'data', 'headers']
	#template_ext = ()
	#ui_color = '#f4a460'

	NEXT_FIELD = "next"


	@apply_defaults
	def __init__(
		self,
		endpoint,
		method='POST',
		data=None,
		headers=None,
		response_check=None,
		extra_options=None,
		xcom_push=False,
		http_conn_id='http_default',
		log_response=False,
		*args, **kwargs
	):

		if (not headers):
			raise ValueError('headers argument is expected, found None.')

		if (not data):
			raise ValueError('data argument is expected, found None.')

		super(TwitterHttpOperator, self).__init__(*args, **kwargs)
		self.http_conn_id = http_conn_id
		self.method = method
		self.endpoint = endpoint
		self.headers = headers #or {}
		self.data = data #or {}
		self.response_check = response_check
		self.extra_options = extra_options or {}
		self.xcom_push_flag = xcom_push
		self.log_response = log_response


	def execute(self, context):

		http = HttpHook(self.method, http_conn_id=self.http_conn_id)

		response_list = []

		
		next_token = None
		first_call = True
		c = 0

		condition = ( (next_token == None) or (first_call) )

		while (condition):

			if (first_call):   

				# updating while loop (partial) condition
				first_call = False

			else:    
				
				# updating dynamic settings before making 2nd, 3rd, ..., nth calls
				if (next_token != None):
					data_json = json.loads(self.data)
					data_json[TwitterHttpOperator.NEXT_FIELD] = next_token
					self.data = json.dumps(data_json)

			try:
				
				# command

				c += 1
				self.log.info("Calling HTTP method : # {0}".format(c))                

				response = http.run(
					endpoint = self.endpoint,
					data = self.data,
					headers = self.headers,
					extra_options = self.extra_options
				)
                
				rs = response.json()


			except Exception as ex:
				self.log.warning('Unknown error : ', str(ex))
				break

			else:

				#if self.log_response:
				#	self.log.info(response.text)

				# appending each document within the response object to list

				if ('results' in rs):
					if (isinstance(rs['results'], list)):
						self.log.info('HTTP method : # {0} |   {1} records.'.format(c, len(rs['results'])))
						for resp in rs['results']:
							response_list.append(resp)
                
				# updating while loop condition

				if (TwitterHttpOperator.NEXT_FIELD in rs):
					next_token = rs[TwitterHttpOperator.NEXT_FIELD]
				else:
					next_token = None

				condition = ( (next_token != None) & (c <= 5) )

				self.log.info('HTTP method : # {0} |   {1} total records  |  next : {2}  |  continue : {3}'.format(c, len(response_list), next_token, condition))

			
		# end of while loop

		response = json.dumps(response_list)

		if self.response_check:
			if not self.response_check(response):
				raise AirflowException("Response check returned False.")

		if self.xcom_push_flag:
			return str(response)
 
		
        
		