# dag
from airflow import DAG


# hooks
from custom_hooks.mongo_hook import MongoHook

# operators
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator


# utils
#from custom_utils.alphavantage_utils import AlphaVantageHelper


# others
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago




# dag

default_args = {
    'owner': 'me',
    'depends_on_past': False,
    'start_date': days_ago(0),
    #'retries': 1,
    #'retry_delay': timedelta(minutes = 5)
}

dag = DAG(
	'layer2_format_twitter', 
	default_args = default_args,
	schedule_interval = None
	)


#mongodb

MONGO_COLLECTION = 'twitter'

MONGO_CONN_ID_INPUT  = 'mongo_cloud_layer1'
MONGO_URI_INPUT      = MongoHook(conn_id = MONGO_CONN_ID_INPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION)

MONGO_CONN_ID_OUTPUT = 'mongo_cloud_layer2'
MONGO_URI_OUTPUT     = MongoHook(conn_id = MONGO_CONN_ID_OUTPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION)


# spark 

t_spark_operator = SparkSubmitOperator(
	task_id = "spark_operator",
	dag = dag,
	application = '/home/team3/.local/bin/dags/layer_2/twitter/twitter_format_pyspark.py',
	conf = {
		'spark.mongodb.input.uri'  : MONGO_URI_INPUT,
		'spark.mongodb.output.uri' : MONGO_URI_OUTPUT
	},
	conn_id = 'spark_master',
	packages = 'org.mongodb.spark:mongo-spark-connector_2.11:2.4.1',
	name = 'airflow-spark',
	verbose = False
)


# trigger

def conditionally_trigger(context, dag_run_obj):
    return dag_run_obj


t_trigger_operator = TriggerDagRunOperator(
    task_id = 'trigger',
    dag = dag,
    trigger_dag_id = 'layer3_augment_twitter',
    python_callable = conditionally_trigger
)


# dependencies

t_spark_operator >> t_trigger_operator