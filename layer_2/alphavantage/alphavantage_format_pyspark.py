from pyspark import SparkContext

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *

from custom_utils.pyspark_utils import PySparkHelper

from custom_hooks.mongo_hook import MongoHook


# debug 

WRITE_OUTPUT = True
WRITE_INPUT = True


# mongodb

MONGO_COLLECTION = 'msft'

MONGO_CONN_ID_SYNC = 'mongo_cloud_layer1'
MONGO_URI_SYNC     = MongoHook(conn_id = MONGO_CONN_ID_SYNC).get_uri() + '.{0}'.format(MONGO_COLLECTION)

MONGO_CONN_ID_OUTPUT = 'mongo_cloud_layer2'
MONGO_URI_OUTPUT     = MongoHook(conn_id = MONGO_CONN_ID_OUTPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION)

print('MONGO_URI_SYNC   : ', MONGO_URI_SYNC)
print('MONGO_URI_OUTPUT : ', MONGO_URI_OUTPUT)


# pyspark helper

psh = PySparkHelper(debug = True, date_field = 'date')


# spark session

spark = SparkSession \
	.builder \
	.appName("airflow_spark_layer2_alphavantage") \
	.getOrCreate()


# spark context

sc = spark.sparkContext


# spark dataframe

print()
print('reading ...')

df = spark.read.format('mongo').load()
df_target = spark.read.format('mongo').option('uri', MONGO_URI_OUTPUT).load()

if (not df.rdd.isEmpty()):
	
	print()
	print('df.shape : ({0},{1})'.format(df.count(), len(df.columns)))
	print('df_target.shape : ({0},{1})'.format(df_target.count(), len(df_target.columns)))
	

	# filtering 

	df = psh.filter_synced(df)

	if (not df.rdd.isEmpty()):

		# converting 		

		convert_dict = {
			'DecimalType' : ['open', 'high', 'low', 'close'],
			'IntegerType' : ['volume'],
			'BooleanType' : ['synced']
		}

		df2 = psh.convert(df, convert_dict)


		# selecting

		cols = ['_id']
		mode = PySparkHelper.SELECT_MODE_EXCLUDE

		df2 = psh.select(df2, cols, mode)

		
		if (not df2.rdd.isEmpty()):

			# join (input + target)

			df3 = psh.join(df2, df_target)		

			#print()
			#df3.printSchema()

			#print()
			#df3.show()			


			# writing (output collection)

			if (WRITE_OUTPUT):	
				psh.update_target(df3, df_target)	


			# writing (input collection)

			if (WRITE_INPUT):
				df = psh.sync_prep(df)
				psh.update_input(df, MONGO_URI_SYNC)			