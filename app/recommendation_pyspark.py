from pyspark import SparkContext

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *

from custom_utils.pyspark_utils import PySparkHelper
from custom_utils.app_utils import AppHelper

from custom_hooks.mongo_hook import MongoHook

import pandas as pd
import datetime


# debug 

WRITE_OUTPUT = True


# mongodb

MONGO_COLLECTION_INPUT_1  = 'time_series'
MONGO_CONN_ID_INPUT_1     = 'mongo_cloud_layer6'
MONGO_URI_INPUT_1         = MongoHook(conn_id = MONGO_CONN_ID_INPUT_1).get_uri() + '.{0}'.format(MONGO_COLLECTION_INPUT_1)

MONGO_COLLECTION_INPUT_2  = 'correlation'
MONGO_CONN_ID_INPUT_2     = 'mongo_cloud_app'
MONGO_URI_INPUT_2         = MongoHook(conn_id = MONGO_CONN_ID_INPUT_2).get_uri() + '.{0}'.format(MONGO_COLLECTION_INPUT_2)

MONGO_COLLECTION_OUTPUT = 'recommendation'
MONGO_CONN_ID_OUTPUT    = 'mongo_cloud_app'
MONGO_URI_OUTPUT        = MongoHook(conn_id = MONGO_CONN_ID_OUTPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION_OUTPUT)

print('MONGO_URI_INPUT_1   : ', MONGO_URI_INPUT_1)
print('MONGO_URI_INPUT_2   : ', MONGO_URI_INPUT_2)
print('MONGO_URI_OUTPUT    : ', MONGO_URI_OUTPUT)


# app helper

ah = AppHelper(debug = True)

# pyspark helper

psh = PySparkHelper(debug = True, date_field = None)


# spark session

spark = SparkSession \
	.builder \
	.appName("airflow_spark_app_recommendation") \
	.getOrCreate()


# spark context

sc = spark.sparkContext


# spark dataframe

print()
print('reading ...')

df = spark.read.format('mongo').option('uri', MONGO_URI_INPUT_1).load()
df_corr = spark.read.format('mongo').option('uri', MONGO_URI_INPUT_2).load()
df_target = spark.read.format('mongo').option('uri', MONGO_URI_OUTPUT).load()


if (
		( not df_corr.rdd.isEmpty() )
		& ( not df.rdd.isEmpty() )
	):

	print()
	print('df.shape        : ({0},{1})'.format(df.count(), len(df.columns)))
	print('df_corr.shape   : ({0},{1})'.format(df_corr.count(), len(df_corr.columns)))
	print('df_target.shape : ({0},{1})'.format(df_target.count(), len(df_target.columns)))


	# filtering by date

	max_lag = df_corr.agg({'lag' : 'max'}).collect()[0]['max(lag)']
	#date_min = pd.to_datetime(
#		pd.Timestamp(datetime.date.today()) - pd.Timedelta(days = max_lag),
#		format = '%Y-%m-%d', 
#		exact = True
#	)
	date_min = ( pd.Timestamp(datetime.date.today()) - pd.Timedelta(days = max_lag) ).strftime('%Y-%m-%d')

	print()
	print('recommendation_pyspark >> max_lag  : ', max_lag)
	print('recommendation_pyspark >> date_min : ', date_min)
	
	df = psh.filter_by_date(
		df = df, 
		date_field = 'date_str', 
		date_min = date_min, 
		date_max = None,
		mode = PySparkHelper.FILTER_DATE_MODE_AFTER
	)

	# filter by engagement

	if (not df.rdd.isEmpty()):

		print()
		print('filtering by engagement ...')

		df = df.filter(df['engagement'] > 0)

		print()
		print('filtering by engagement >> df.shape = ({0},{1})'.format(df.count(), len(df.columns)))

		print()
		print(df)

		if (not df.rdd.isEmpty()):			

			# selecting

			cols = [
				'date_str',
				'close',
				'engagement'
			]
			mode = PySparkHelper.SELECT_MODE_INCLUDE

			df2 = psh.select(df, cols, mode)


			# pandas dataframe

			print()
			print('converting to : Pandas DataFrame ...')

			df_corr = df_corr.toPandas()
			
			df = df.toPandas()
			df['date'] = pd.to_datetime(df['date_str'], format = '%Y-%m-%d', exact = True)
			df['close'] = df['close'].astype(float).round(2)
			df['engagement'] = df['engagement'].astype(float).round(2)


			# indexing

			df = df.set_index(keys = 'date', drop = False, inplace = False)


			# reindexing			

			start = df['date'].min()
			end = df['date'].max()

			print()
			print('start : ', start)
			print('end   : ', end)

			idx = pd.date_range(
				start = start,
				end = end,
				freq = 'D',
				tz = None
			)

			df = df.reindex(idx)	

			# recommendation

			print()
			print('computing recommendation ...')

			df['engagement_level'] = df.apply(lambda x: ah.get_engagement_level(x), axis = 1)
			recomm_dict = ah.recommendation_test_01(df, df_corr)


			# spark dataframe

			print()
			print('converting to : Spark DataFrame ...')

			rdd = sc.parallelize([recomm_dict])			
			df4 = spark.read.json(rdd)

				
			if (not df4.rdd.isEmpty()):
			
				# writing (output collection)

				if (WRITE_OUTPUT):	
					psh.append_target(df4, df_target)			

				
				