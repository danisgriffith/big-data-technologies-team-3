from pyspark import SparkContext

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *

from custom_utils.pyspark_utils import PySparkHelper
from custom_utils.app_utils import AppHelper

from custom_hooks.mongo_hook import MongoHook

import pandas as pd
import datetime


# debug 

WRITE_OUTPUT = True


# mongodb

MONGO_COLLECTION_SYNC = 'time_series'
MONGO_CONN_ID_SYNC    = 'mongo_cloud_layer6'
MONGO_URI_SYNC        = MongoHook(conn_id = MONGO_CONN_ID_SYNC).get_uri() + '.{0}'.format(MONGO_COLLECTION_SYNC)

MONGO_COLLECTION_OUTPUT = 'correlation'
MONGO_CONN_ID_OUTPUT    = 'mongo_cloud_app'
MONGO_URI_OUTPUT        = MongoHook(conn_id = MONGO_CONN_ID_OUTPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION_OUTPUT)

print('MONGO_URI_SYNC   : ', MONGO_URI_SYNC)
print('MONGO_URI_OUTPUT : ', MONGO_URI_OUTPUT)


# app helper

ah = AppHelper(debug = True)

# pyspark helper

psh = PySparkHelper(debug = True, date_field = None)


# spark session

spark = SparkSession \
	.builder \
	.appName("airflow_spark_app_correlation") \
	.getOrCreate()


# spark context

sc = spark.sparkContext


# spark dataframe

print()
print('reading ...')

df = spark.read.format('mongo').load()
df_target = spark.read.format('mongo').option('uri', MONGO_URI_OUTPUT).load()


if ( not df.rdd.isEmpty() ):

	print()
	print('df.shape : ({0},{1})'.format(df.count(), len(df.columns)))
	print('df_target.shape : ({0},{1})'.format(df_target.count(), len(df_target.columns)))


	# filtering by date

	date_min = str(int(datetime.date.today().year) - 3) + '01-01'
	df = psh.filter_by_date(
		df = df, 
		date_field = 'date_str', 
		date_min = date_min, 
		date_max = None,
		mode = PySparkHelper.FILTER_DATE_MODE_AFTER
	)

	if (not df.rdd.isEmpty()):	

		# selecting

		cols = [
			'date_str',
			'close',
			'engagement'
		]
		mode = PySparkHelper.SELECT_MODE_INCLUDE

		df2 = psh.select(df, cols, mode)


		# pandas dataframe

		print()
		print('converting to : Pandas DataFrame ...')

		df3 = df2.toPandas()
		df3['date'] = pd.to_datetime(df3['date_str'], format = '%Y-%m-%d', exact = True)
		df3['close'] = df3['close'].astype(float).round(2)
		df3['engagement'] = df3['engagement'].astype(float).round(2)


		# indexing

		df3 = df3.set_index(keys = 'date', drop = False, inplace = False)


		# reindexing			

		start = df3['date'].min()
		end = df3['date'].max()

		print()
		print('start : ', start)
		print('end   : ', end)

		idx = pd.date_range(
			start = start,
			end = end,
			freq = 'D',
			tz = None
		)

		df3 = df3.reindex(idx)	


		# correlation (LOW-LEVEL TWEETS)
		print('correlation_pyspark : ', df3.columns)
		df_corr_low = ah.compute_correlation(
			df = df3,
			dict_t = AppHelper.DICT_LOW,
			engagement_level = 'low',
			method = AppHelper.CORRELATION_MODE_PEARSON,
			debug = False
		)
		print()
		print('df_corr_low :  >> df.shape = ({0},{1})'.format(len(df_corr_low), len(df_corr_low.columns)))


		# correlation (MID-LEVEL TWEETS)

		df_corr_mid = ah.compute_correlation(
			df = df3,
			dict_t = AppHelper.DICT_MID, 
			engagement_level = 'middle',
			method = AppHelper.CORRELATION_MODE_PEARSON,
			debug = False
		)
		print()
		print('df_corr_mid :  >> df.shape = ({0},{1})'.format(len(df_corr_mid), len(df_corr_mid.columns)))


		# correlation (HIGH-LEVEL TWEETS)

		df_corr_high = ah.compute_correlation(
			df = df3,
			dict_t = AppHelper.DICT_HIGH, 
			engagement_level = 'high',
			method = AppHelper.CORRELATION_MODE_PEARSON,
			debug = False
		)
		print()
		print('df_corr_high :  >> df.shape = ({0},{1})'.format(len(df_corr_high), len(df_corr_high.columns)))


		# correlation (ALL-LEVEL TWEETS)

		df_corr = pd.DataFrame()
		df_list = [df_corr_low, df_corr_mid, df_corr_high]
		df_list = [df_temp for df_temp in df_list if (not df_temp.empty)]

		df_corr = pd.concat(objs = df_list, axis = 0, ignore_index = True)

		print()
		print('df_corr_low  :  >> df.shape = ({0},{1})'.format(len(df_corr_low), len(df_corr_low.columns)))
		print('df_corr_mid  :  >> df.shape = ({0},{1})'.format(len(df_corr_mid), len(df_corr_mid.columns)))
		print('df_corr_high :  >> df.shape = ({0},{1})'.format(len(df_corr_high), len(df_corr_high.columns)))	
		print('df_corr      :  >> df.shape = ({0},{1})'.format(len(df_corr), len(df_corr.columns)))


		# spark dataframe

		print()
		print('converting to : Spark DataFrame ...')

		df4 = spark.createDataFrame(df_corr)

			
		if (not df4.rdd.isEmpty()):
		
			# writing (output collection)

			if (WRITE_OUTPUT):	
				psh.update_target(df = df4, df_target = df_target, overwrite = True)	

			

			
			