# dag
from airflow import DAG


# hooks
from custom_hooks.mongo_hook import MongoHook
from airflow.hooks.base_hook import BaseHook


# sensors
from custom_sensors.mongo_sensor import MongoSensor


# operators
from airflow.operators.python_operator import PythonOperator
from airflow.operators.email_operator import EmailOperator

# utils
from custom_utils.app_utils import AppHelper

# others
from datetime import datetime, timedelta
from airflow.utils.dates import days_ago

import json




# dag

default_args = {
    'owner': 'me',
    'depends_on_past': False,
    'start_date': days_ago(0),
    #'retries': 1,
    #'retry_delay': timedelta(minutes = 5)
}

dag = DAG(
    'app_presentation', 
    default_args = default_args, 
    schedule_interval = None
    )


# mongodb

MONGO_CONN_ID = "mongo_cloud_app"
MONGO_COLLECTION = "recommendation"
DATE_FIELD = "exec_date_str"

hook = MongoHook(MONGO_CONN_ID)    


def read_push(**kwargs):

    cursor = hook.find(
        mongo_collection = MONGO_COLLECTION, 
        query = {DATE_FIELD: datetime.utcnow().date().strftime("%Y-%m-%d")},
        find_one = False
    )
    result = [doc for doc in cursor]
    for doc in result:
        del doc['_id']
    print(type(result))
    print(result)

    return result


# email

def pull_send(**context):
    

    data_pulled = context['ti'].xcom_pull(
        dag_id = 'app_presentation', 
        task_ids = 'python_operator_push'
    )    

    if (data_pulled):
        print(data_pulled)
        print('presentation : sending email ...')
        ah = AppHelper(debug = True)

        html_content = ah.build_recommendation_html(recommendation_list = data_pulled)

        email_op = EmailOperator(
            task_id = 'send_email',
            to = ["danis.griffith@studenti.unitn.it", "marco.tonin@studenti.unitn.it"],
            subject = "Big Data Technologies - Daily Recommendation",
            html_content = html_content
        )
        email_op.execute(context)
    else:
        print('presentation : no data to be sent')


t_mongo_operator_push = PythonOperator(
    task_id = 'python_operator_push',
    provide_context = True,
    python_callable = read_push,
    dag = dag,
)

t_mongo_operator_send = PythonOperator(
    task_id = 'python_operator_send',
    provide_context = True,
    python_callable = pull_send,
    dag = dag,
)


# dependencies

t_mongo_operator_push >> t_mongo_operator_send
