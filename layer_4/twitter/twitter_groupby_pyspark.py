from pyspark import SparkContext

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *

from custom_utils.pyspark_utils import PySparkHelper

from custom_hooks.mongo_hook import MongoHook

import pandas as pd
import pytz


# debug 

WRITE_OUTPUT = True
WRITE_INPUT = True


# mongodb

MONGO_COLLECTION = 'twitter'

MONGO_CONN_ID_SYNC = 'mongo_cloud_layer3'
MONGO_URI_SYNC     = MongoHook(conn_id = MONGO_CONN_ID_SYNC).get_uri() + '.{0}'.format(MONGO_COLLECTION)

MONGO_CONN_ID_OUTPUT = 'mongo_cloud_layer4'
MONGO_URI_OUTPUT     = MongoHook(conn_id = MONGO_CONN_ID_OUTPUT).get_uri() + '.{0}'.format(MONGO_COLLECTION)

print('MONGO_URI_SYNC   : ', MONGO_URI_SYNC)
print('MONGO_URI_OUTPUT : ', MONGO_URI_OUTPUT)


# pyspark helper

psh = PySparkHelper(debug = True, date_field = 'date_str')


# spark session

spark = SparkSession \
	.builder \
	.appName("airflow_spark_layer4_twitter") \
	.getOrCreate()


# spark context

sc = spark.sparkContext


# spark dataframe

print()
print('reading ...')

df = spark.read.format('mongo').load()
df_target = spark.read.format('mongo').option('uri', MONGO_URI_OUTPUT).load()


if (not df.rdd.isEmpty()):

	print()
	print('df.shape : ({0},{1})'.format(df.count(), len(df.columns)))
	print('df_target.shape : ({0},{1})'.format(df_target.count(), len(df_target.columns)))


	# filtering 

	df = psh.filter_synced(df)

	if (not df.rdd.isEmpty()):	

		# pandas dataframe

		print()
		print('converting to : Pandas DataFrame ...')

		df2 = df.toPandas()


		# group by

		print()
		print('group by ...')

		cols = [
			'date_str',
			'date_day', 
			'date_weekend',
			'year',
			'month',
			'year_month',
			'quote_count',
			'reply_count',
			'retweet_count',
			'favorite_count',
			'engagement',
			'synced'
		]

		by = [
			'date_str',
			'date_day', 
			'date_weekend',
			'year',
			'month',
			'year_month',
			'synced'
		]

		df2 = df2[cols].groupby(by = by, axis = 0, as_index = False).sum()
		

		# spark dataframe

		print()
		print('converting to : Spark DataFrame ...')

		df2 = spark.createDataFrame(df2)


		# converting (data types are correct from the beginning)

		# ...


		# selecting

		cols = ['_id']
		mode = PySparkHelper.SELECT_MODE_EXCLUDE

		df3 = psh.select(df2, cols, mode)
		

		if (not df3.rdd.isEmpty()):

			# join (input + target)

			df4 = psh.join(df3, df_target)		

			
			# writing (output collection)

			if (WRITE_OUTPUT):	
				psh.update_target(df4, df_target)				


			# writing (input collection)

			if (WRITE_INPUT):
				df = psh.sync_prep(df)
				psh.update_input(df, MONGO_URI_SYNC)